# coding=utf-8
from __future__ import division
__author__ = 'jody'

import numpy as np
import random
import time
import string
import Utils as utl
import os
import math

class Creator:
    """
    ATTENZIONE: i parametri possono essere diversi
    da quelli trovati in fase di valutazione!

    es. posso creare dizionari con 100 parole e poi
    usare solo 15 parole per documento; l'informazione
    sul dizionario originale viene persa.
    """
    def __init__(self,
                 # FEATURES
                 n_classes,
                 n_documents,
                 n_uniqueWords4class, #in media
                 n_words4doc, #in media
                 n_words4dic, # in media
                 # ALTRO
                 n_commonWords,
                 variability #in percentuale
                 ):
        """
        :param n_classes: numero di classi
        :param n_documents: numero di documenti
        :param n_uniqueWords4class: numero di parole
            uniche per ogni classe in media
        :param n_words4doc: numero di parole medio per
            documento
        :param n_words4dic: numero di parole medio
            per dizionario
        :param n_commonWords: numero di parole che possono
            essere prese dai vari dizionari come parole
            in comune
        :param variability: il discostamento dalla
            media per tutti le features medie
        :return:
        """

        self._n_classes = n_classes
        self._n_documents = n_documents
        self._n_uniqueWords4class = n_uniqueWords4class
        self._n_words4doc = n_words4doc
        self._n_words4dic = n_words4dic
        self._n_commonWords = n_commonWords
        self._variability = variability

    def perform(self):
        classes = np.empty(self._n_classes,
                           dtype="|S10")
        for i in xrange(self._n_classes):
            classes[i] = ("CLASS_%i" %i)

        print classes

        # ogni doc nel formato docDset
        # es. "[CLASS]fdsafasdfsafsa"
        documents = []

        common_dic = np.empty(self._n_commonWords,
                              dtype="|S10")
        for i in xrange(self._n_commonWords):
            common_dic[i] = self._getRandomWord()

        #print(common_dic)

        # non è garantito che le parole nei vari dic
        # non siano affatto presenti nel common, ma
        # ci si affida alla bassa probabilita'
        for c in classes:
            dic = np.empty(self._n_words4dic, dtype='|S10')

            # AGGIUNGO LE PAROLE UNICHE

            for i in xrange(self._n_uniqueWords4class):
                try:
                    dic[i] = self._getRandomWord()
                except:
                    print("*** Error adding uniqueWords4class ***")
                    print("i: %s" %str(i))
                    print("_n_uniqueWords4class: %s" %str(self.
                                                        _n_uniqueWords4class ))
                    print("_n_words4dic: %s") %str(self._n_words4dic)
                    pass

            # AGGIUNGO LE PAROLE COMUNI
                for i in xrange(self._n_uniqueWords4class, self._n_words4dic):
                    try:
                        dic[i] = random.choice(common_dic)
                    except:
                        print("*** Error adding uniqueWords4class ***")
                        print("i: %s" %str(i))
                        print("_n_uniqueWords4class: %s" %str(self.
                                                        _n_uniqueWords4class ))
                        print("_n_words4dic: %s") %str(self._n_words4dic)
                        pass

            #AGGIUNGO I DOCUMENTi
            n_docs_to_append = int(round(self._n_documents / self._n_classes))
            for i in xrange(n_docs_to_append):
                documents.append(Creator._getRandomDoc(dic,
                                                       self._n_words4doc,
                                                       c))
        return documents


    @staticmethod
    def _getRandomWord():
        n_letters = random.randint(3,8)
        word = ""
        for i in xrange(n_letters):
            letter = random.choice(string.ascii_lowercase)
            word += letter
        return word

    @staticmethod
    def _getRandomDoc(dictionary, n_words4doc, class_label):
        """
        restituisce un documento del tipo:
        "[CLASS_LABEL]parole parole parole"
        """
        doc = "[%s]" % class_label
        for i in xrange(n_words4doc):
            doc += (random.choice(dictionary) + " ")

        return doc

    """
    @staticmethod
    def _getInterval(mean, sd):

        99,7% = P{ μ - 3,00 σ < X < mean + 3,00 σ }
        :param mean:
        :param sd:
        :return:

        return np.array([mean-3*sd, mean+3*sd])
    """

    @staticmethod
    def _getInterval(mean, variability):
        lower = mean*(1-variability)
        upper = mean*(1+variability)
        return np.array([lower, upper])

    @staticmethod
    def get_randomParameters(name):

        n_classes = random.randint(2, 15)
        n_documents = random.randint(100, 1000)
        n_uniqueWords4class = random.randint(0,10)
        n_words4doc = random.randint(30,300)
        n_words4dic = random.randint(1, 600)
        n_commonWords = n_words4doc - n_uniqueWords4class
        variability = None #per ora non viene usata

        # asserzioni sui parametri
        assert(n_uniqueWords4class + n_commonWords == n_words4doc)
        assert(n_uniqueWords4class < n_words4doc)

        dic = {"name": name, "n_classes": n_classes, "n_documents": n_documents,
               "n_uniqueWords4class": n_uniqueWords4class, "n_words4doc": n_words4doc,
               "n_words4dic": n_words4dic, "n_commonWords": n_commonWords,
               "variability": variability}

        return dic


def writeOne(name, n_classes, n_documents, n_uniqueWords4class, n_words4doc,
             n_words4dic, n_commonWords, variability):

    c = Creator(n_classes, n_documents, n_uniqueWords4class, n_words4doc,
                n_words4dic, n_commonWords, variability)

    documents = c.perform()

    path = utl.dbName2file(name, suffix="txt")
    directory = utl.dbName2path(name)

    if not os.path.exists(directory):
        os.makedirs(directory)
    with open(path, "w") as f:
        for doc in documents:
            f.write(doc + "\n")


def writeOne_random(name):

    parameters = Creator.get_randomParameters(name)

    writeOne(name=parameters["name"],
             n_classes=parameters["n_classes"],
             n_documents=parameters["n_documents"],
             n_uniqueWords4class=parameters["n_uniqueWords4class"],
             n_words4doc=parameters["n_words4doc"],
             n_words4dic=parameters["n_words4dic"],
             n_commonWords=parameters["n_commonWords"],
             variability=parameters["variability"]
             )


def __develop__():
    pass


def main():
    n_dataset_toCreate = 500
    k = 0
    for i in xrange(n_dataset_toCreate):
        k += 1
        print("--- " + str(k) + " of " + str(n_dataset_toCreate) + " ---")
        writeOne_random("autoDS_"+str(i))


if __name__ == "__main__":
    tik = time.time()
    main()
    tok = time.time()
    print("[Execution time: %f" %(tok-tik))