__author__ = 'jody'

import pandas as pd
import numpy as np
from sklearn import tree, cross_validation

def perform():
    # carica il dataset
    df = pd.read_csv('experiments_results.csv', sep=';')
    
    # groupby per avere il best score
    dfg = df.groupby("name", as_index=False)
    dfg_max = dfg.max()
    dfg_max = dfg_max[["name","score"]]
    
    # faccio il merge per avere gli altri attributi insieme al best
    # N.B. in caso di valori con best score pari,
    #      vengono tenute tutte le trasformazioni con quello score
    df_merge = pd.merge(df, dfg_max, on=["name","score"], how="inner")
    
    # poiche' ho trasformazioni diverse dello stesso dataset che sono
    # a pari merito com best score, voglio pulire il dataset tenendo solo
    # quelle senza pari merito, conto allora quante diverse trasformazioni
    # hanno ottenuto il best score per lo stesso dataset
    def contaRipetzioni(x):
        x["Repetitions"] = x["name"].count()
        return x
    
    df_with_repetitions = df_merge.groupby("name").apply(contaRipetzioni)
    
    # restituisce un array di booleani che faranno da filtro
    filter = np.array(df_with_repetitions["Repetitions"] == 1L)
    
    ds = df_with_repetitions[filter]
    
    # svuoto la memoria
    del df, dfg_max, df_with_repetitions
    
    # definisco la table degli attributi di apprendimento (X)
    not_attributes = ["name", "transformation", "algorithm", "score"]
    attributes = [i for i in df_merge.columns if i not in not_attributes]
    X = np.array(ds[attributes])
    print(X)
    
    # definisco la lista degli attributi
    Y = np.array(ds["transformation"])
    print(Y)
    
    print("Shape of the dataset (train + test): %s" %str(X.shape))
    
    n_examples = X.shape[0]
    p_training_examples = 0.75
    n_training_examples = int(n_examples*p_training_examples)
    n_test_examples = n_examples - n_training_examples
    
    print("Training size: %i" %n_training_examples)
    print("Test size: %i" %n_test_examples)
    
    train_X = X[0:n_training_examples]
    train_Y = Y[0:n_training_examples]
    test_X = X[n_training_examples:]
    test_Y = Y[n_training_examples:]
    
    # definizione del modello
    model = tree.DecisionTreeClassifier()
    # addestramento
    model.fit(train_X,train_Y)
    
    # lo uso per la predizione
    # model.predict(test)
    predicted = model.predict_proba(test_X)
    
    print(predicted)
    
    try:
        print(predicted.classes_)
    except:
        pass


if __name__ == "__main__":
    perform()
