[calcio] Balotelli ha segnato un gran gol
[scuola] la maestra punisce gli allievi discoli
[calcio] la nazionale italiana vince nella partita in trasferta
[scuola] Ai docenti è lasciata discrezionalità. come afferma il preside: "Qualcuno può indicare libri di testo, che però non devono essere nuovi ma quelli utilizzati negli scorsi anni" oppure, e questa è la linea preferita, "preparare dispense o aprire blog per fornire ai ragazzi tutto il supporto necessario
[calcio] Il successo più prestigioso resta quella dell’anno scorso a Milano contro l’Inter, sfida decisa da un gol in extremis di Emiliano Moretti: gli altri al Sant’Elia contro il Cagliari (2-1 grazie a Glik e Quagliarella) e due stagioni fa ad Udine per merito di Immobile e Farnerud
[scuola] Tutte le scuole statali, paritarie e non paritarie di ogni ordine e grado del comune di Torino. 
[calcio] Il gioco di squadra aumenta le probabilità di vittoria
[scuola] Per il corretto apprendimento è importante che gli allievi studino ogni giorno
[calcio] una sconfitta che i giocatori della Juventus non dimenticheranno facilmente
[scuola] In aula non si fuma
[calcio] Il calciomercato chiude a fine agosto, presto sapremo quale sarà la nuova squadra di Balotelli
[scuola] Le sessioni di esame incominceranno la prossima settimana. Gli studenti dovranno munirsi di documenti.
[calcio] Un allenatore di qualità sa tenere unita la squadra.
[scuola] Il telefonino non è ammesso in classe
[calcio] L'arbitro non è stato equo nel giudicare gli episodi della partita
[scuola] Gli incontri genitori insegnanti sono fondamentali per tenere sotto controllo l'andamento dei figli