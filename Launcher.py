import Auto_data_creator
import FeaturesExtractor
import Experiments
import MetaAnalizer
import time

tik = time.time()
#Auto_data_creator.main()
FeaturesExtractor.extract_from_all()
Experiments.perform_all()
#MetaAnalizer.perform()
tok = time.time()
print("")
print("Execution time: %i minutes" %int((tok-tik)/60))
