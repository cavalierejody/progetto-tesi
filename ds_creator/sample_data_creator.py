import random
import string

#PARAMETERS
n_docs = 150
min_words_for_doc = 20
max_words_for_doc = 30

animals = ['dog', 'cat', 'bird', 'whale', 'rat']
colors = ['red', 'range', 'yellow', 'green', 'blue', 'purple']
geometry = ['square', 'circle', 'triangle', 'rectangle', 'ellipse']

arguments = [animals, colors, geometry]

def getArgumentName(arg):
    if arg is animals:
        return "[ANIMALS]"
    elif arg is colors:
        return "[COLORS]"
    elif arg is geometry:
        return "[GEOMETRY]"

def getRandomWord():
    n_letters = random.randint(3,8)
    word = ""
    for i in xrange(n_letters):
        letter = random.choice(string.ascii_lowercase)
        word += letter
    return word


documents = []




for i in xrange(n_docs):
    arg = random.choice(arguments)
    doc = getArgumentName(arg)
    num_words = random.randint(min_words_for_doc,
                               max_words_for_doc)
    for j in xrange (num_words):
        word = random.choice(arg)
        doc += word + " "
    documents.append(doc)

with open("./docset1/docset1.txt", "w") as f:
    for doc in documents:
        f.write(doc+"\n")

print documents


