__author__ = 'jody'

import os
import pickle
import re
from collections import defaultdict
from gensim import corpora, models, similarities

# FUNZIONI DI IO

def get_dirs(path):
    """
    restituisce tutte le cartelle, anche nascoste,
    contenute nel primo livello di un certo path
    :param path: string
    :return:
    """
    return [name for name in os.listdir(path)
            if os.path.isdir(
            os.path.join(path,name))]

def dbName2path(name):
    """
    trasforma il nome di un db nel suo path corrispondente
    in base a quanto previsto dal framework
    :param name: "iris"
    :return: "./data/iris/"
    """
    return "./data/%s/" % name

def dbName2file(name, prefix="", suffix=""):
    """
    :param name: "iris"
    :param prefix: "BOW"
    :param suffix: "txt"
    :return: "./data/iris/BOW__iris.txt"
    """
    #controlli per evitare che si stampino i delimitatori
    #anche senza prefissi o suffissi
    if prefix != "":
        prefix = prefix + "__"
    if suffix != "":
        suffix = "." + suffix

    return dbName2path(name) + prefix + name + suffix

def splitLabeledString(inputString):
    """
    :param inputstring: "[DANTE]nel mezzo del cammin di nostra vita"
    :return: list ["DANTE", "nel mezzo del...]
    """
    assert (inputString[0] == "["),\
        "no valid format; expected \"[\" as first char"

    endCharIndexForLabel = inputString.find("]")
    assert (endCharIndexForLabel != -1),\
        "no valid format; expected \"]\" char"

    return [inputString[1:endCharIndexForLabel],
            inputString[endCharIndexForLabel+1:]]

def clean_string(line):
    return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", line).split()).upper()

def read_from_CSV(self, delimitator):
    """
    legge il db da file assumendo che sia un csv con delimitatore
    """
    raise NotImplementedError


def read_from_TXT(file, clean):
    """
    legge il db da file assumendo che sia un txt con un documento di tipo
    string per ogni riga

    Example of txt:
    NON-LABELED
    ----------
    a saper ben maneggiare le gride nessuno e' reo, nessuno innocente
    nel mezzo del cammin di nostra vita mi ritrovai per una selva oscura
    si sta come d'autunno sugli alberi le foglie
    ----------

    LABELED
    [MANZONI]a saper ben maneggiare le gride nessuno e' reo, nessuno innocente
    [DANTE]nel mezzo del cammin di nostra vita mi ritrovai per una selva oscura
    [UNGARETTI]si sta come d'autunno sugli alberi le foglie


    Parameters
    ----------
    file: string
        es.: "./data/foo.txt"
    clean : boolean

    Return
    ----------
    dictrionary {labels, documents}
    """

    print("[trying to read from: %s...]" % file)


    with open(file, 'rb') as f:
        print("[reading from: %s...]" % file)

        documents = []
        labels = []

        for line in f:
            try:
                split = splitLabeledString(line)
                label = split[0]
                text = split[1]
                labels.append(label)
            except:
                text = line

            if clean:
                documents.append(clean_string(text))
            else:
                documents.append(text)


        print("[%i documents read]" % len(documents))


        return {"documents": documents, "labels": labels}


# FUNZIONI DI PULIZIA E INIZIALIZZAZIONE GENSIM

def apply_stopList(documents, stopList):
    """

    :param documents: una lista di parole,
        ogni item un documento come string
    :param stopList:
    :return: una nuova lista di liste di parole, una per documento
    """

    if stopList == "ITA" or stopList == "IT":
        return apply_stopList(documents, "./stoplists/ita.stoplist")
    elif stopList == "ENG" or stopList == "EN":
        return apply_stopList(documents, "./stoplists/eng.stoplist")
    else:
        stopwords = []
        try:
            sl = open(stopList, 'rb')
            stopwords = [stopword.rstrip().upper() for stopword in sl]
            print("[stoplist loaded from %s]" % stopList)
        except:
            print("[no stoplist loaded]")

        output = [[word for word in document.split() if word not in stopwords]
                  for document in documents]

        return output


def get_occurrency_dict(documents):
    """
    :param documents: una lista di liste di parole, una per documento
    :return: un dizionario parole -> occorrenze
    """

    occurrencies = defaultdict(int)
    #le parole vanno messe in una lista, non in una stringa
    for doc in documents:
        for token in doc:
            occurrencies[token] += 1
    return occurrencies


def remove_rare_words(documents, occurrency_dict, min_occurr):
    """

    :param documents: lista di liste di parole, una per documento
    :param occurrency_dict: ottenuto da get_occurrency_dict()
    :param min_occurr: numero minimio di occorrenze per
        tenere una parola
    :return: nuovo documents che con solo parole che abbiano
        almeno min_occurr (>=)
    """
    if min_occurr == 0:
        return [[token for token in doc] for doc in documents]
    else:
        return [[token for token in doc if occurrency_dict[token] > min_occurr]
            for doc in documents]


#TODO: rimuovere e sostituire con codice inline
def create_dictionary(documents):
    """

    :param documents: lista di liste di parole, una per documento
    :return: dictionary object di gensim
        per stamparlo utilizzare:
            pprint(dictionary.token2id)
    """
    dictionary = corpora.Dictionary(documents)
    return dictionary

def load_DSets(path=None):
    """
    carica la lista di tutte le cartelle presenti
    nel path, ciascuna dell quali contiene un db
    :param path: il path dove cercare i dsets
    :return: una lista di stringhe dei path
    """
    if path == None:
        path = "./data"

    return get_dirs(path)


if __name__ == "__main__":
    print("debug")

    path = "./data/letteratura/letteratura.txt"
    #path = "./data/test/test.txt"
    o = read_from_TXT(path, clean=True)
    print o