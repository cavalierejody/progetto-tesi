# coding=utf-8


import Utils as utl
# from scipy.sparse import dok_matrix
from pprint import pprint
import gensim as gs
import numpy as np
import xml.etree.ElementTree as ET
from sklearn.decomposition import PCA


# funzioni per il DocDataset che non sono vere trasformazioni
# nel senso matematico di passaggio da una base all'altra

def vectorize(docDs, stoplist, min_occur):
    """
    prende un docDs con documents come lista di stringhe
    e lo ritorna modifica come lista di liste di parole

    non reversibile!!!

    applica una stoplist a un docDs e
    rimuove parole con occorenze < di min_occur

    :param docDs: DocDataset
    :param stoplist: "ENG", "ITA", o lista (vedi funzione)
    :param min_occur: numero minimio di occorrenze per
        tenere una parola
    :return: nothing
    il docDs ha subito la trasformazione
    """

    documents = docDs.documents
    documents = utl.apply_stopList(documents, stoplist)
    occurrency_dic = utl.get_occurrency_dict(documents)
    documents = utl.remove_rare_words(documents,
                                      occurrency_dic,
                                      min_occur)
    docDs.documents = documents


# classi di trasformazione
# ogni trasformazione dovrà riportare un nuovo oggetto
# per evitare problemi di clone

class Transf():
    name = "TRANSF"

    def __str__(self):
        return self.name

    def __init__(self, docDs):
        self._perform(docDs)

    @staticmethod
    def _perform(docDs):
        raise NotImplementedError

class PCA_T(Transf):
    """
    Funziona solo con matrici dense, requisito sklearn
    """
    name = "PCA"

    @staticmethod
    def _perform(docDs):
        BOW_T(docDs, type="dense")
        documents = np.array(docDs.documents)
        matrix = PCA().fit_transform(documents)
        docDs.documents = matrix
        #del documents

        headers = []
        for i in xrange(matrix.shape[1]):
            headers.append("ATTR_%i" %i)
        docDs.headers = headers

        return {"headers": headers,
                "matrix": matrix}



class BOW_T(Transf):
    name = "BOW"

    def __init__(self, docDs, type="sparse"):
        """

        :param docDs:
        :param type: "dense" o "sparse"
        :return: numpy.array o scipy.sparse in base al type
        """
        self._perform(docDs, type)

    @staticmethod
    def _perform(docDs, type):
        """
        :param type: "sparse" o "dense"
        :return: numpy.array o scipy.sparse in base al type
        """
        documents = docDs.documents
        #rimuovere utl.create_dictrionary e sostiture con codic inline
        dictionary = utl.create_dictionary(documents)
        bow_corpus = [dictionary.doc2bow(doc) for doc in documents]

        if type == "dense":
            #matrice standard numpy
            matrix = gs.matutils.corpus2dense(bow_corpus,
                                              len(dictionary))
        elif type == "sparse":
        #matrice scipy.sparse
            matrix = gs.matutils.corpus2csc(bow_corpus)

        matrix = matrix.T
        docDs.documents = matrix

        headers = []
        for i in xrange(len(dictionary)):
            headers.append(str(dictionary[i]))
        docDs.headers = headers

        return {"dictionary": dictionary,
                "corpus": bow_corpus,
                "headers": headers,
                "matrix": matrix}

class TFIDF_T(Transf):
    name = "TFIDF"

    @staticmethod
    def _perform(docDs):

        documents = docDs.documents
        dictionary = utl.create_dictionary(documents)
        bow_corpus = [dictionary.doc2bow(doc) for doc in documents]
        tfidf = gs.models.TfidfModel(bow_corpus)
        tfidf_corpus = tfidf[bow_corpus]

        #TODO: aggiungere l'implementazione dense/sparse come da BOW
        #matrix = gs.matutils.corpus2dense(tfidf_corpus,
        #                                  len(dictionary))

        matrix = gs.matutils.corpus2csc(tfidf_corpus)
        matrix = matrix.T
        docDs.documents = matrix

        headers = []
        for i in xrange(len(dictionary)):
            headers.append(str(dictionary[i]))
        docDs.headers = headers

        return {"dictionary": dictionary,
                "corpus": tfidf_corpus,
                "headers": headers,
                "matrix": matrix}

class LSI_T(Transf):
    name = "LSI"

    @staticmethod
    def _perform(docDs):

        documents = docDs.documents
        dictionary = utl.create_dictionary(documents)
        bow_corpus = [dictionary.doc2bow(doc) for doc in documents]
        num_topics=docDs.getNClasses()

        tfidf = gs.models.TfidfModel(bow_corpus)
        tfidf_corpus = tfidf[bow_corpus]
        lsi = gs.models.LsiModel(tfidf_corpus,
                              id2word=dictionary,
                              num_topics=num_topics)
        lsi_corpus = lsi[tfidf_corpus]

        matrix = gs.matutils.corpus2dense(lsi_corpus,
                                          num_topics)
        matrix = matrix.T
        docDs.documents = matrix

        headers = []
        for i in xrange(num_topics):
            headers.append("TOPIC_%i" %i)
        docDs.headers = headers

        return {"dictionary": dictionary,
                "corpus": lsi_corpus,
                "headers": headers,
                "matrix": matrix}

class LDA_T(Transf):
    name = "LDA"

    @staticmethod
    def _perform(docDs):

        documents = docDs.documents
        dictionary = utl.create_dictionary(documents)
        bow_corpus = [dictionary.doc2bow(doc) for doc in documents]
        num_topics=docDs.getNClasses()

        tfidf = gs.models.TfidfModel(bow_corpus)
        tfidf_corpus = tfidf[bow_corpus]
        lda = gs.models.LdaModel(tfidf_corpus,
                              id2word=dictionary,
                              num_topics=num_topics)
        lda_corpus = lda[tfidf_corpus]

        matrix = gs.matutils.corpus2dense(lda_corpus,
                                          num_topics)
        matrix = matrix.T
        docDs.documents = matrix

        headers = []
        for i in xrange(num_topics):
            headers.append("TOPIC_%i" %i)
        docDs.headers = headers

        return {"dictionary": dictionary,
                "corpus": lda_corpus,
                "headers": headers,
                "matrix": matrix}




class DocDataset():
    """
    classe per la rappresentazione di un dataset di testi
    a colonne non fisse (a differenza dei .csv)

    ogni trasformazione applicata sara' non reversibile
    per evitare di occupare memoria
    """

    def __init__(self, db_name):
        """
        :param db_name: string
            il nome della cartella in cui è presente il file,
            deve essere contenuto nella cartella ./data/"dataFolder"/
        """

        self.name = db_name
        self.root_path = utl.dbName2path(db_name)
        self.labels = None
        self.documents = None
        self.headers = None
        self.language = None

    def init_descriptor_XML(self):

        file = utl.dbName2file(self.name,
                               suffix="xml")

        try:
            print("[reading descriptor from %s]"
                  %file)
            tree = ET.parse(file)
            root = tree.getroot()

            #inizializzazione degli attributi da xml
            self.language = root.get("language")
        except:
            #raise NameError("[unable to read %s]" % file)
            pass


    def init_from_TXT(self, clean=True):
        """
        importa i documenti da un file .txt
        :clean: pulisce il testo levando punteggiatura, accenti
            e mettendo tutto maiuscolo

        :return: Nothing
        """
        #TODO: gestire headers

        file = utl.dbName2file(self.name, suffix="txt")
        imported = utl.read_from_TXT(file, clean)
        self.documents = imported["documents"]
        self.labels = imported["labels"]

    def __str__(self):
        sampleSize = 10
        try:
            documentsSize = len(self.documents)
            if documentsSize < 10:
                sampleSize = documentsSize
            docs_out = self.documents[0:sampleSize]
        except:
            docs_out = self.documents

        return str(("Name: " + self.name,\
               "Headers: " + str(self.headers),\
               "Documents: " + str(docs_out),\
               "Labels: " + str(self.labels[0:sampleSize])))

    def getNClasses(self):
        return len(set(self.labels))




if __name__ == "__main__":
    pass
