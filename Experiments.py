__author__ = 'jody'

import os
import Utils as utl
import Transformations as trf
import copy as cp
from sklearn.cluster import KMeans
from sklearn import metrics
import time
from pprint import pprint
import json
from FeaturesExtractor import Features_Generator
import csv
import numpy as np
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.cross_validation import train_test_split



def perform_all():
    # carico di datasets da ./data

    dsets_list = utl.load_DSets()
    print("Lista dei datasets:")
    print(dsets_list)

    transf_list = [trf.BOW_T, trf.TFIDF_T, trf.LSI_T, trf.LDA_T, trf.PCA_T]

    scores = {}
    table_results = []
    headers_0 = ["name", "transformation", "algorithm", "score"]
    features_names = Features_Generator.getAll_features_names()
    headers = headers_0 + features_names
    table_results.append(headers)

    k_ds_evaluating = 0
    for ds_name in dsets_list:

        #TODO: usare le np.array sempre dove possibile
        k_ds_evaluating += 1
        print("--- " + str(k_ds_evaluating) + " of " + str(len(dsets_list)) + " ---")

        #importa il ds da lasciare intonso in modo da
        #effettuare le trasformazioni sempre su una sua copia
        ds_default = trf.DocDataset(ds_name)
        ds_default.init_from_TXT()
        ds_default.init_descriptor_XML()
        ds_features = None
        with open(utl.dbName2path(ds_name)+"features.json", "r")as f:
            ds_features = json.load(f)

        #print("\tlanguage: " + str(ds_default.language))
        #print("\t" + str(ds_default.documents))
        #print("\t" + str(ds_default.labels))

        #TODO: controllare se non c'e' lingua
        trf.vectorize(ds_default, ds_default.language, 1)

        ds_score = {}


        for transf in transf_list:

            #TODO: trasformo il db con cache
            #tr_ds = cached(transf(ds).perform())

            # copia il ds_default (non necessita init_from_TXT)
            ds = cp.deepcopy(ds_default)

            transf(ds)
            print ("\t[SVM performing with %s transformation]" %transf.name)

            #print(ds.headers)
            #print(ds.documents)
            #print(ds)

            #k_means = KMeans(ds.getNClasses(),
            #                 init='k-means++',
            #                 max_iter=300,
            #                 verbose=0)


            #k_means.fit(ds.documents)

            X_train, X_test, y_train, y_test = train_test_split(ds.documents,
                                                        ds.labels,
                                                        random_state=0)
            
            classifier = svm.SVC()
            y_pred = classifier.fit(X_train, y_train).predict(X_test)
            
            try:
                #score = metrics.adjusted_rand_score(ds.labels,
                #                            k_means.labels_)

                score = accuracy_score(y_test, y_pred)

                ds_score[transf.name] = score

                features_list = []
                for i in features_names:
                    features_list.append(ds_features[i])

                line = [ds_name, transf.name, "SVM", score] + \
                       features_list
                table_results.append(line)

            except:
                print "\t[Unable to verifiy]"
                if ds.labels == [] or ds.lables is None:
                    print "\tNo true labels loaded"

        scores[ds.name] = ds_score

    with open("results.json", "w") as o_f:
        json.dump(scores, o_f, indent=2)


    pprint(scores)

    with open("experiments_results.csv", "w") as output:
        writer = csv.writer(output, delimiter=";", lineterminator='\n')
        writer.writerows(table_results)


if __name__ == "__main__":
    tik = time.time()
    perform_all()
    tok = time.time()
    print("[Execution time: %f" %(tok-tik))






