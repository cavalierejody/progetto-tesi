from __future__ import division

__author__ = 'jody'

import Transformations as trf
import numpy as np
import time
import Utils as utl
import json
from pprint import pprint
import time

from scipy.sparse import dok_matrix
from scipy.stats import bernoulli
import matplotlib as mlp
from matplotlib import pyplot
import random as rnd
import math
import gensim as gn
import sklearn as sk
from sklearn.cross_validation import train_test_split
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix
from sklearn.decomposition import PCA
from sklearn import tree, cross_validation
import pandas as pd
from pprint import pprint
import os.path
import warnings
warnings.filterwarnings('ignore')

"""
SIGNIFICATI DELLE FEATURES
        features = {"n_words4dic":numero di parole che costituiscono il dizionario di ciascuna classe (usate per generare un documento casuale di una determinata classe),
                    "n_documents":numero di documenti totale (ovvero numero di esempi del dataset),
                    "n_classes":numero di classi,
                    "n_words4doc":numero di parole per ogni documento, (ovvero numero di colonne del dataset),
                    "n_uniqueWords4class":numero di parole presenti solo nei documenti di una determinata classe, e non negli altri documenti di altre classi (es. le parole Promessi e Sposi sono presenti solo nella classe dei documenti che parlano di Manzoni),


                    "r_classes_docs":rapporto numero classi su numero di documenti,
                    "r_wordsDic_docs":rapporto numero parole dei dizionari su numero di documenti,
                    "r_words4doc_wordsDic":rapporto numero di parole per documento su numero di parole per dizionario
                    }
"""


class Features_Generator:
    """
    tutti i metodi di calcolo delle features devono iniziare con 'calc_'
    per poter essere chiamati da getAll_features
    """
    def __init__(self, table, labels):
        self.table = table
        print(self.table)
        self.labels = labels
        self._Allstd = None
        self._corr_matricies_4class = None
        self._total_variations = None
    @staticmethod
    def __methodName2transformationName(mathodName):
        """
        trasforma il nome di un metodo nel nome della feature
        es.: "calc_foo" -> "foo"
        """
        return mathodName[5:]
    @staticmethod
    def getAll_features_names():
        methods = [Features_Generator.__methodName2transformationName(m) \
                   for m in Features_Generator._getAll_features_methods() ]
        return methods
    @staticmethod
    def _getAll_features_methods():
        methods = [m for m in dir(Features_Generator) if m[:5] == 'calc_' ]
        return methods
    def getAll_features(self):
        features = {}
        for f_method in self._getAll_features_methods():
            f_name = Features_Generator.__methodName2transformationName(f_method)
            features[f_name] = getattr(self, f_method)()
        return features
    def calc_n_documents(self):
        """
        numero di documenti (esempi)
        """
        try:
            return self.table.shape[0]
        except:
            pass
        try:
            return len(self.table)
        except:
            raise
    def calc_n_attributes(self):
        """
        numero di attributi
        """
        return self.table.shape[1]
    def calc_n_classes(self):
        """
        numero di classi
        """
        return len(set(self.labels))

    def get_corr_matricies_4class(self):
        if (self._corr_matricies_4class is not None):
            return self._corr_matricies_4class
        else:
            corr_matricies = []
            for aClass in set(self.labels):
                indices = (self.labels == aClass)
                if np.sum(indices) == 1:
                    return 1
                class_records = self.table[indices]
                corr_matricies.append(np.corrcoef(class_records))
                self._corr_matricies_4class = corr_matricies
            return corr_matricies  
    def calc_n_bicorr_80(self):
        """
        il numero di coppie di attributi con correlazione > 0.8 o < -0.8
        """
        n_over_bound_overall = []
        """
        for aClass in set(self.labels):
            indices = (self.labels == aClass)
            class_records = self.table[indices]
            corr_matrix = np.corrcoef(class_records)
        """
        for corr_matrix in self.get_corr_matricies_4class():
            n_over_bound = len(np.where(corr_matrix > 0.8)[0]) -\
                                        len(corr_matrix)
            n_over_bound += len(np.where(corr_matrix < -0.8)[0])
            n_over_bound_overall.append(n_over_bound/2)
        return np.mean(n_over_bound_overall)
    def calc_n_bicorr__20(self):
        """
        il numero di coppie di attributi con correlazione in [-0.2, 0.2]
        """
        n_over_bound_overall = []
        for corr_matrix in self.get_corr_matricies_4class():
            n_over_bound = len(np.where(corr_matrix < .2)[0])
            n_over_bound -= len(np.where(corr_matrix < -.2)[0])
            n_over_bound_overall.append(n_over_bound)
        return np.mean(n_over_bound_overall)

    def getAll_std(self):
        if (self._Allstd is not None):
            return self._Allstd
        else:
            stds = []
            for i in xrange(self.table.shape[0]):
                stds.append(np.std(self.table[:][i]))
            self._Allstd = stds
            return stds
    def calc_std_mean_attributes(self):
        """
        calcola la deviazione standard media degli attributi/colonne
        """
        return np.asscalar(np.mean(self.getAll_std()))
    def calc_std_max_attributes(self):
        """
        calcola la deviazione standard max degli attributi/colonne
        """
        return np.asscalar(np.max(self.getAll_std()))
    def calc_std_min_attributes(self):
        """
        calcola la deviazione standard min degli attributi/colonne
        """
        return np.asscalar(np.min(self.getAll_std()))
        
    
    def calc_r_attributes_documents(self):
        """
        calcola il rapporto n_attributes/n_documents
        """
        return self.calc_n_attributes()/self.calc_n_documents()
    def calc_r_classes_documents(self):
        """
        calcola il rapporto n_classes/n_documents
        """
        return self.calc_n_classes()/self.calc_n_documents()
    def calc_r_classes_attributes(self):
        """
        calcola il rapporto n_classes/n_attributes
        """
        return self.calc_n_classes()/self.calc_n_attributes()
    def calc_i_Simpson(self):
        """
        l'indice di Simpson rappresenta la prob. che 2 documenti
        presi a caso appartengano alla stessa classe
        """
        n = len(self.labels)
        frequencies4class = []
        for aClass in set(self.labels):
            indices = (self.labels == aClass)
            frequencies4class.append(np.sum(indices)/n)
        return np.sum(p*p for p in frequencies4class)
    def calc_r_words4doc_documents(self):
        """
        calcola il numero medio di parole usate in un documento su
        numero di documenti
        """
        words4docs = []
        for doc in self.table:
            words4docs.append(np.sum(doc>0))
        return np.mean(words4docs)
    def calc_n_apax_01(self):
        """
        calcola il numero di parole che compaiono con una frequenza
        inferiore a 01 all'interno del corpus
        """
        sum_Allwords = np.sum(self.table)
        frequencies4words = self.table.sum(axis=0)
        return np.sum(frequencies4words/sum_Allwords < 0.01)

def extract_from_all():
    """
    estrae le features per ogni ds in ./data/
    salva su json tali features
    :return:
    """

    # carico di datasets da ./data
    start_main_time = time.time()

    dsets_list = utl.load_DSets()
    print("Lista dei datasets:")
    print(dsets_list)

    dsets_features = {}

    k_ds_evaluating = 0
    for ds_name in dsets_list:

        k_ds_evaluating += 1
        print("--- " + str(k_ds_evaluating) + " of " + str(len(dsets_list)) + " ---")


        #importa il ds da lasciare intonso in modo da
        #effettuare le trasformazioni sempre su una sua copia
        ds = trf.DocDataset(ds_name)
        ds.init_from_TXT()
        ds.init_descriptor_XML()

        trf.vectorize(ds, ds.language, 1)

        trf.BOW_T(ds, "dense")


        features_generator = Features_Generator(np.array(ds.documents),
                                                np.array(ds.labels))

        features = features_generator.getAll_features()
        #features = FeatureExtractor.getAll(ds)

        features_file = ds.root_path + "features.json"
        with open(features_file, "w") as f:
            json.dump(features, f, indent=2)

        dsets_features[ds.name] = features

    with open("dsets_features.json", "w") as f:
        json.dump(dsets_features, f, indent=2)
    print("[dsets_features:]")
    pprint(dsets_features)

    return dsets_features


if __name__ == "__main__":
    tik = time.time()
    extract_from_all()
    tok = time.time()
    print("[Execution time: %f]"%(tok-tik))
